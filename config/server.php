<?php

use Sws\App;

return [
    'http' => [                                    //命令启动的服务名
        'host' => '0.0.0.0',                       //监听地址
        'port' => 9001,                            //监听端口
        'mode' => SWOOLE_PROCESS,                  //运行模式 默认为SWOOLE_PROCESS
        'sock_type' => SWOOLE_SOCK_TCP,            //sock type 默认为SWOOLE_SOCK_TCP
        'setting' => [                             //当前server配置项，会覆盖公共设置
            'pid_file' => App::getInstance()->getRootPath() . 'http.pid',  //每个服务独立的文件，切记不可重名
            'log_file' => App::getInstance()->getRootPath() . 'http.log',
            'worker_num' => swoole_cpu_num(),
            'daemonize' => false,
        ],
        'event' => [
            'onRequest' => [\Sws\Server\Http::class, 'onRequest']
        ],
        'exceptionHandle' => ''                    //HTTP请求下异常处理,默认由框架处理，可以手动接管
    ],
    'websocket' => [
        'host' => '0.0.0.0',                       //监听地址
        'port' => 9002,                            //监听端口
        'mode' => SWOOLE_PROCESS,                  //运行模式 默认为SWOOLE_PROCESS
        'sock_type' => SWOOLE_SOCK_TCP,            //sock type 默认为SWOOLE_SOCK_TCP
        'setting' => [                             //当前server配置项，会覆盖公共设置
            'pid_file' => App::getInstance()->getRootPath() . 'websocket.pid',  //每个服务独立的文件，切记不可重名
            'log_file' => App::getInstance()->getRootPath() . 'websocket.log',
            'daemonize' => false,
        ],
        'event' => []
    ]

];