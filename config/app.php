<?php

return [
    // 调试模式
    'debug' => true,
    "php-ini" => [
        // 默认时区
        'default_timezone' => 'Asia/Shanghai',
        // 设置PHP最大内存
        'memory_limit' => '1G',
    ]
];
