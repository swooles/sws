<?php

namespace App\Command;

use Sws\Console\Command;
use Sws\Console\Console;

class Hello extends Command
{
    /**
     * 配置指令
     * @return void
     */
    function configure()
    {
        $this->setName('hello')
            ->addArgument('name', Command::OPTIONAL, 'you name', 'sws')
            ->addOption('age', 'a', Command::OPTIONAL, 'you age', 0)
            ->setDesc('This is a test command.');
    }

    /**
     * 执行指令
     * @return void
     */
    function execute()
    {
        $name = 'sws';
        if ($this->hasArgument('name')) {
            $name = $this->getArgument('name');
        }

        $age = 0;
        if ($this->hasOption('age')) {
            $age = $this->getOption('age');
        }

        $this->console->writeln(sprintf("Hello,My name is %s,I am %d years old.", $name, $age));
    }
}