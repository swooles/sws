<?php

namespace App\Controller;

use Swoole\Http\Request;
use Swoole\Http\Response;

class Index
{
    /**
     * @param Request $request
     * @param Response $response
     * @return array
     */
    public function index(Request $request, Response $response): array
    {
        return [
            'Hello Sws.',
            'time' => date('Y-m-d H:i:s')
        ];
    }

}