<?php

use Sws\App;

$route = App::getInstance()->route;

$route->get('/', [\App\Controller\Index::class, 'index']);

$route->get('/favicon.ico', function () {
    return '';
});


